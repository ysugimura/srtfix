package srtfix;

import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;

import javax.swing.*;

public class Main {

  public static void main(String[]args) {
    new Main().execute();
  }

  private SrtFix srtFix = new SrtFix();
  
  private void execute() {
    JFrame frame = new JFrame();
    JLabel label = new JLabel("Drop SRT here");
    setupDrop(label);
    frame.add(label);
    frame.setSize(400, 200);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);;
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
  }
  
  private void setupDrop(JComponent component) {
    DropTargetListener dtl = new DropTargetAdapter() {
      @Override public void dragOver(DropTargetDragEvent dtde) {
        if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
          dtde.acceptDrag(DnDConstants.ACTION_COPY);
          return;
        }
        dtde.rejectDrag();
      }
      @Override public void drop(DropTargetDropEvent dtde) {
        try {
          if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            dtde.acceptDrop(DnDConstants.ACTION_COPY);
            Transferable transferable = dtde.getTransferable();
            
            @SuppressWarnings("unchecked")
            List<Object> list = 
              (List<Object>)transferable.getTransferData(DataFlavor.javaFileListFlavor);
            
            list.stream()
              .map(o->(File)o)            
              .filter(file->file.exists())            
              .map(file->file.toPath())
              .forEach(path-> {
                try {
                  srtFix.fix(path); 
                } catch (IOException ex) {
                  throw new RuntimeException(ex);
                }
             });            
            
            dtde.dropComplete(true);
            return;
          }
        } catch (UnsupportedFlavorException | IOException ex) {
          ex.printStackTrace();
        }
        dtde.rejectDrop();
      }
    };
    new DropTarget(component, DnDConstants.ACTION_COPY, dtl, true);
  }
}
