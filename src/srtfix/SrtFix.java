package srtfix;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;

public class SrtFix {


  void fix(Path path) throws IOException {
    
    // 行を取得
    List<String>lines = Files.readAllLines(path);
    
    // タグをドロップ
    lines = lines.stream().map(line->dropTags(line)).collect(Collectors.toList());

    // おかしなパターンを修正
    lines = lines.stream().map(line->fixInvalidPattern(line)).collect(Collectors.toList());
    
    Files.write(path, lines);
  }
  
  static Pattern INVALID_PATTERN = Pattern.compile("^(.+) --> 00:00:00,000$");
  
  private String fixInvalidPattern(String line) {
    Matcher m = INVALID_PATTERN.matcher(line);
    if (!m.matches()) return line;
    return m.group(1) + " --> " + m.group(1);
  }
  
  static Pattern TAG_PATTERN = Pattern.compile("(<[^>]*>)");
  
  String dropTags(String line) {
    return dropTags(line, TAG_PATTERN);
  }

  /**
   * 文字列の中から指定パターンに一致する部分を削除する
   * @param line 対象文字列
   * @param pattern パターン
   * @return 削除済文字列
   */
  public static String dropTags(String line, Pattern pattern) {
    StringBuilder dropped = new StringBuilder();
    int start = 0;
    Matcher m = TAG_PATTERN.matcher(line);    
    while (m.find()) {
      dropped.append(line.substring(start, m.start()));
      start = m.end();
    }
    if (start < line.length()) 
      dropped.append(line.substring(start));    
    return dropped.toString();
  }
}
